#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"
#include "string.h"

#define MY_UUID { 0x70, 0x65, 0x36, 0xA5, 0x58, 0xFE, 0x4D, 0x5D, 0xAF, 0xBE, 0x66, 0x81, 0x76, 0x81, 0x43, 0xFF }
PBL_APP_INFO(MY_UUID,
             "TheCommandline", "ColonDashStar",
             1, 0, /* App version */
             RESOURCE_ID_IMAGE_THECMD_ICON,
             APP_INFO_WATCH_FACE);

Window window;
TextLayer Line[9];
TextLayer Title;
Animation anim;
Animation anim_date;
AnimationImplementation animimp;
AnimationImplementation animimp_date;

static char Line_Text[9][30] = {
    "pbl$ date +%a..0",
    "pbl$ date +%a..1",
    "pbl$ date +%a..2",
    "pbl$ date +%a..3",
    "pbl$ date +%a..4",
    "pbl$ date +%a..5",
    "pbl$ date +%a..6",
    "pbl$ date +%a..7",
    "pbl$ date +%a..8"
    };
/* String Storage
pbl$
pbl$ date +%k:%M
Di MAY-07 Wk 19
pbl$ date +%a...
*/
static char Title_Text[] = "Terminal";
static char time_cmd[] =        "pbl$ date +%k:%M";
static char time_cmd_12h[] =    "pbl$ date +%I:%M %P";
//static char time_cmd_12h[] =    "pbl$ date +%I:%M>";
static char date_cmd[] =        "pbl$ date +%a %b-%d Wk %V";
static char next_cmd[30] =        "pbl$ date +%a %b-%d Wk %V";
static int Step = 0;
static GFont Custom_Font;
static int ani_stepper = 0;

//void anim_teardown(struct Animation *animation);
void anim_callback(struct Animation *animation,const uint32_t time);
void writeLines(int curstep);
void writeDisplay();


void anim_teardown(struct Animation *animation) {
 (void)animation;
    writeLines(Step);
    Step++;
    writeDisplay();    
}

void anim_callback(struct Animation *animation,const uint32_t time){
	(void)animation;
    int ani_step = 0;   
    ani_step = (int)(((time/100) * (uint32_t)strlen(next_cmd)) / (uint32_t)655);
    if (ani_step != ani_stepper) {
        ani_stepper = ani_step;
        if (ani_step < 4) ani_step = 4;
        strcpy(Line_Text[0],next_cmd);
        Line_Text[0][ani_step] = '\0';
        //Scrolling
        if (strlen(Line_Text[0]) > 17) {
            memmove(Line_Text[0], &Line_Text[0][strlen(Line_Text[0])-17], 18);
        }
        text_layer_set_text(&Line[0], Line_Text[0]);
    } 
    /*
    Integer Debug
    static char buff[10] = {};
    int i=0;
    int temp_num;
    int length;
    length = 0;
    temp_num = ani_step;
    while(temp_num) {
        temp_num /= 10;
        length++;
    }
    if (length == 0)  {
        strcpy(buff,"nutting");
        text_layer_set_text(&Line[7],  buff);
        return;
    }
    for(i = 0; i < length; i++) {
        buff[(length-1)-i] = '0' + (ani_step % 10);
        ani_step /= 10;
    }    
    buff[i] = '\0';
    text_layer_set_text(&Line[7], buff );*/
}

void writeDisplay() {
    int i=0;    
    for (i=0;i<9;i++) {
        if (strlen(Line_Text[i]) > 17) {
            Line_Text[i][17] = 62;
            Line_Text[i][18] ='\0';
        }
        text_layer_set_text(&Line[i], Line_Text[i]);
    }    
}

void writeLines(int curstep) {
    char *time_format;
    //If we initialize, clear screen
    if (curstep == 0) {
        strcpy(&Line_Text[0][0], "pbl$");
        strcpy(&Line_Text[1][0], "                 ");
        strcpy(&Line_Text[2][0], "                 ");
        strcpy(&Line_Text[3][0], "                 ");
        strcpy(&Line_Text[4][0], "                 ");
        strcpy(&Line_Text[5][0], "                 ");
        strcpy(&Line_Text[6][0], "                 ");
        strcpy(&Line_Text[7][0], "                 ");
        strcpy(&Line_Text[8][0], "                 ");    
        Step++;
        curstep++;
    }
    //Write the next command to Line 0. Just in Case we haven't done it in the animation
    strcpy(Line_Text[0], next_cmd);
    //Move up!
    int g = 0;
    for (g=6;g>-1;g--) {
        strcpy(Line_Text[g+2], Line_Text[g]);            
    }
    //First Line text added here
    strcpy(Line_Text[0], "pbl$");
    //Time added here
    PblTm cur_time;
    get_time(&cur_time);
    if (curstep == 2) {
        time_format = "%a %b-%d Wk %V";
    } else {
        if (clock_is_24h_style()) {
            time_format = "%R";
          } else {
            time_format = "%I:%M %P";
        }
    }
    string_format_time(Line_Text[1], sizeof(Line_Text[1]), time_format, &cur_time);
    //Put in the next command. 
    if (curstep == 1) {
        strcpy(next_cmd, date_cmd);
    } else {
        if (clock_is_24h_style()) {
            strcpy(next_cmd, time_cmd);
        } else {
            strcpy(next_cmd, time_cmd_12h);
        }
    } 
    
}

void handle_minute_tick(AppContextRef ctx, PebbleTickEvent *t) {

    (void)ctx;
    PblTm timenow;
    //After Step 4 got againgto step 1
    if (Step == 5) Step = 1;
    //write the display variables
    writeLines(Step);
    //Next step
    Step++;
    //write the display
    writeDisplay();
    //initiate next animation
    get_time(&timenow);
    //Set time animation
    if (timenow.tm_sec < 50) {
        animation_set_delay(&anim,(57-timenow.tm_sec) * 1000);
        animation_set_duration(&anim,strlen(next_cmd)*100);
        animation_schedule(&anim);
    }
    //Set date animation if appropiate
    if (timenow.tm_sec < 10 && Step == 2) {
        animation_set_delay(&anim_date,(27-timenow.tm_sec) * 1000);
        animation_set_duration(&anim_date,strlen(date_cmd)*100);
        animation_schedule(&anim_date);
    }
}

void handle_deinit(AppContextRef ctx) {
  (void)ctx;
  fonts_unload_custom_font(Custom_Font);
}

void handle_init(AppContextRef ctx) {
  (void)ctx;

  window_init(&window, "TheCommandline");
  window_stack_push(&window, true /* Animated */);
    
   
    //Load resources
    resource_init_current_app(&THECOMMANDLINE_RESOURCES);
    //Load Font
    Custom_Font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ANDALEMONO_14)); 

    //initialize title
    text_layer_init(&Title, window.layer.frame);
    text_layer_set_text_color(&Title, GColorBlack);
    text_layer_set_background_color(&Title, GColorWhite);
    layer_set_frame(&Title.layer, GRect(0, 0, 144, 15));  
    text_layer_set_font(&Title, Custom_Font);
    text_layer_set_text_alignment(&Title, GTextAlignmentCenter);
    text_layer_set_text(&Title, Title_Text);
    layer_add_child(&window.layer, &Title.layer);    
    //Initialize lines
    int t=0;
    for (t=0;t<9;t++) {
      text_layer_init(&Line[t], window.layer.frame);
      text_layer_set_text_color(&Line[t], GColorWhite);
      text_layer_set_background_color(&Line[t], GColorBlack);
      layer_set_frame(&Line[t].layer, GRect(0, 151-(17*t), 144, 17));  
      text_layer_set_font(&Line[t], Custom_Font);
      text_layer_set_text_alignment(&Line[t], GTextAlignmentLeft);
      layer_add_child(&window.layer, &Line[t].layer);         
    }

    //Write initial content
    if (clock_is_24h_style()) {
        strcpy(next_cmd, time_cmd);
    } else {
        strcpy(next_cmd, time_cmd_12h);
    }
    writeLines(Step);
    Step++;
    writeDisplay();
    
    //Set up animation
    animation_init(&anim);
    animation_init(&anim_date);
    animimp.update = &anim_callback;
    //animimp.teardown = &anim_teardown;
    animimp_date.update = &anim_callback;
    animimp_date.teardown = &anim_teardown;
    animation_set_implementation(&anim,&animimp);
    animation_set_implementation(&anim_date,&animimp_date);
    
}


void pbl_main(void *params) {
  PebbleAppHandlers handlers = {
    .init_handler = &handle_init,
    .deinit_handler = &handle_deinit,
    .tick_info = {
      .tick_handler = &handle_minute_tick,
      .tick_units = MINUTE_UNIT
    }
  };

  app_event_loop(params, &handlers);
}
